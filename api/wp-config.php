<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'apiwpv2' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4<gK/WZ0gm:B#T}K8m*DHf/+E9W^6Nk|R4LU%I+x0$mRD&lP@cxZ^S-8c;)>/[L,' );
define( 'SECURE_AUTH_KEY',  'fA:TD%h[6tAQY+ql!SlDj$4pEyWX1@0%=)Z{`%rnVC3p:]8A8:fM+$PR;?2[bW,5' );
define( 'LOGGED_IN_KEY',    'Q8!U$<#rOEK1jJ|SR7.;BiqKLt_yplKA7(aVfH[@G;6Z!qh3|jS0ii- 6&G#8Yyk' );
define( 'NONCE_KEY',        '=z3k!f`OZAyi?SRZ>V{{v8-*D-LYMy~6_C1pJz#Ap(Z~Y=g/e}X @ADW@NMEi1>7' );
define( 'AUTH_SALT',        'l-xx@``0z$OU|Zs.Pi8?t(!og%NcXVNWWid=qZ2i6?#E^Z_<up)+j&yUrs?@&!VF' );
define( 'SECURE_AUTH_SALT', '::v=F2) 3y+jrWv-47yR9GF3VbE $Vu NCA$%LDSIGlBG@IW-Nlsf~)|d:ujxcj0' );
define( 'LOGGED_IN_SALT',   'a%*I`n:/srCO%{3J5agN0BeKF`<E?)#Vr1.%E&:_!?ZN@htgB*$3K(vf(^x>45xu' );
define( 'NONCE_SALT',       '.ht{xl=e%.8P>;/`t=aF0r}b_&g>YUj$;3d+f&/MOtbL./W0[)qr(eQJU-LCW.E|' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
